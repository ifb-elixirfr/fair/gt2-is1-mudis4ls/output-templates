# Documents templates

## Description
Creating DSW document models templates implementing DSW interoperability with DMP-OPIDoR. The implementation is based on the use of [annotations](https://guide.ds-wizard.org/en/latest/about/introduction/knowledge-model.html#annotations) within the DSW knowledge model.

## Roadmap

1. Creating a document model implementing interoperability using the RDA DMP CS [exchange format](https://github.com/RDA-DMP-Common/RDA-DMP-Common-Standard). 
2. Creating a document model implementing interoperability using the DMP-OPIDoR [data dictionnary](https://opidor.github.io/maDMP-OPIDoR_documentation/).

## Documentation

Follow this [link](https://ifb-elixirfr.gitlab.io/fair/gt2-is1-mudis4ls/output-templates/).

## Authors and acknowledgment
Paulette Lieby.

## Developers
Paulette Lieby, Sylvain Milanesi.

## License
GNU GPLv3 

## Project status
Work in progress. Expected completion of the interoperability with RDA DMP CS is end of this year. Interoperability with DMP-OPIDoR may take six additional months.

