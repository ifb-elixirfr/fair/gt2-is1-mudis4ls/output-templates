# Copy of RDA-DMP-CS compatible DMP output in spite of testing by Sylvain

Template exclusivement written for the IFB:pgd-structure:2.3.13+ knowledge model and based on the [RDA DMP Common Standard (DCS)](https://github.com/RDA-DMP-Common/RDA-DMP-Common-Standard) for machine-actionable Data Management Plans and the provided [JSON schema](https://github.com/RDA-DMP-Common/RDA-DMP-Common-Standard/tree/master/examples/JSON/JSON-schema) for machine-actionable DMPs. 

The template uses the fact that the KM's entities (questions, answers to option type questions, ...) are annotated in such a way as to effect the mapping from the KM to the DCS. Theorically, any KM properly annotated in such a fashion could use this templae.

Just 1 output format:
. a representation of the DMP as an annotation tree which includes questionnaire replies if they exist;

Note that output is meant for developers for debugging purposes.

## Issues and Contributing

This document template for DSW is available as open-source via the GitLab Repository [output_templates/interop_using_annotations](https://gitlab.com/ifb-elixirfr/fair/gt2-is1-mudis4ls/output-templates/-/tree/main/interop_using_annotations?ref_type=heads), you can [report issues](https://gitlab.com/ifb-elixirfr/fair/gt2-is1-mudis4ls/output-templates/-/issues) there and fork it for customisations or contributions.

### Contributors


* **Sylvain** <[contact-dsw-if@groupes.france-bioinformatique.fr](mailto:contact-dsw-ifb@groupes.france-bioinformatique.fr)>

## Changelog
### 0.0.2


### 0.0.1

- first version: cleaning and simplification of the original output template
- upgrade to metamodel version 14


## License

This template is released under Apache-2.0. Read the [LICENSE](https://raw.githubusercontent.com/datenzee/rdf-template/main/LICENSE) file for details.
