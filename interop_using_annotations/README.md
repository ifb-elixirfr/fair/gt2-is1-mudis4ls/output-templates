# RDA-DMP-CS Interoperability using DSW annotation features

Template exclusivement written for the IFB:pgd-structure:2.3.13+ knowledge model and based on the [RDA DMP Common Standard (DCS)](https://github.com/RDA-DMP-Common/RDA-DMP-Common-Standard) for machine-actionable Data Management Plans and the provided [JSON schema](https://github.com/RDA-DMP-Common/RDA-DMP-Common-Standard/tree/master/examples/JSON/JSON-schema) for machine-actionable DMPs. 

The template uses the fact that the KM's entities (questions, answers to option type questions, ...) are annotated in such a way as to effect the mapping from the KM to the DCS. Theorically, any KM properly annotated in such a fashion could use this templae.

There are three different outputs provided by the template:

1. the json output of the DMP in a DCS-compliant format;
2. a representation of the DMP as an annotation tree which includes questionnaire replies if they exist;
3. a list of child-parent relationships represented as a dictionnary { child_uuid : [ parent_uuid ] }.

Note that outputs 2. and 3. are meant for developers for debugging purposes.

## Issues and Contributing

This document template for DSW is available as open-source via the GitLab Repository [output_templates/interop_using_annotations](https://gitlab.com/ifb-elixirfr/fair/gt2-is1-mudis4ls/output-templates/-/tree/main/interop_using_annotations?ref_type=heads), you can [report issues](https://gitlab.com/ifb-elixirfr/fair/gt2-is1-mudis4ls/output-templates/-/issues) there and fork it for customisations or contributions.

### Contributors


* **Paulette Lieby** <[contact-dsw-if@groupes.france-bioinformatique.fr](mailto:contact-dsw-ifb@groupes.france-bioinformatique.fr)>
  * ORCID: [0000-0002-9289-9652](https://orcid.org/0000-0002-9289-9652)
  * GitLab: [@paule00](https://gitlab.com/paule00)


## Changelog

### 1.1.3


### 1.1.2

- metamodelVersion 16

### 1.1.1

- metamodelVersion 14

### 1.1.0

- fixed a few nasty bugs (principally syntax errors...)
- start on update for annotations key-value pair re-engineering

### 1.0.2

- fix README

### 1.0.1

- bug re empty DMP now fixed

### 1.0.0

- first successful rda-dmp-json compliant export and import into DMP-OPIDoR (using annotations)

### 0.1.7

- ach... boulette dans la 0.1.6

### 0.1.6

- now, two outputs: either the dmp, or the annotation tree
- the dmp is generated using the hierarchy embedded in the tree


### 0.1.5

- PoC for annotation tree output (with replies if any)

### 0.1.4

- re-engineering work put on hold

### 0.1.3

- metamodelVersion 12

### 0.1.2

- created a file for global variables, and reorganised their use in a cleaner fashion

### 0.1.1

- output proper interoperable json object { "dmp": .... }

### 0.1.0

- first version: contributor, contactdone, and bits of dataset


## License

This template is released under Apache-2.0. Read the [LICENSE](https://raw.githubusercontent.com/datenzee/rdf-template/main/LICENSE) file for details.
