{%- import "rda_dmp_cs/_utils.j2" as utils with context -%}


{%- set local_ns = namespace(description="distribution") -%}

{%- set local_ns.infrastructure_name_item_path = none -%}
{%- set local_ns.infrastructure_site_name_item_path = none -%}




{%- macro processRdaInfrastructureName(globals, reply) -%}
  {#- we'll accept only one infrastructure: -#}
  {#- it makes not sense to describe more than one, especially since we -#}
  {#- allow the infrastructure to have more than one physical site,-#} 
  {#- see below  -#}
  
  {%- if globals.global_ns.distribution_vars.infrastructure_title -%}
    {{ utils.warningMessage(false, "in processRdaInfrastructureName, only one infrastructure will be described") }}
  {%- else -%}
    {%- do globals.global_ns.distribution_vars.update({ "infrastructure_title" : reply }) -%} 

    {#- we need to record the item path for that reply -#}
    {#- for further comparison -#}
    {{ utils.getLastItemPath(globals) }}
    {%- set item = utils.global_ns.var_stack.pop() -%}
    {%- set local_ns.infrastructure_name_item_path = item -%}
  {%- endif -%} 
{%- endmacro -%} 


{%- macro processRdaInfrastructureAcronym(globals, reply) -%}
  {{ utils.checkIsSubItem(globals, local_ns.infrastructure_name_item_path, 1) }}
  {%- set answer = utils.global_ns.var_stack.pop() -%}
  
  {%- if answer is true -%}
    {#- if globals.global_ns.distribution_vars.infrastructure_title -#}
    {#- has already a value, that's fine, we overwrite with the acronym -#}
    {#- this may change in the future  -#}
    {%- do globals.global_ns.distribution_vars.update({ "infrastructure_title" : reply }) -%}
  {%- else -%}
    {#- reply is ignored -#}
  {%- endif -%}

{%- endmacro -%}


{%- macro processRdaInfrastructureURL(globals, reply) -%}
  {#- we can accept only one URL, the first documented -#} 
  {#- our model allows for more -#} 
  
  {%- if globals.global_ns.distribution_vars.infrastructure_url -%}
    {#- reply is ignored -#}
  {%- else -%}
    {#- we'll compare item_path with local_ns.infrastructure_item_name_path -#}
    {#- the former must be a superset of the latter  -#}
    
    {{ utils.checkIsSubItem(globals, local_ns.infrastructure_name_item_path, 1) }}
    {%- set answer = utils.global_ns.var_stack.pop() -%}
    {%- if answer is true -%}
      {%- do globals.global_ns.distribution_vars.update({ "infrastructure_url" : reply }) -%} 
      {#- else: we do nothing, since only one infrastructure item block -#}
      {#- should exist -#}
    {%- endif -%}
  {%- endif -%}
{%- endmacro -%}


{%- macro processRdaInfrastructurePhysicalSiteName(globals, reply) -%}
  {#- we'll compare item_path with local_ns.infrastructure_item_name_path -#}
  {#- the former must be a superset of the latter  -#}

  {{ utils.checkIsSubItem(globals, local_ns.infrastructure_name_item_path, 1) }}
  {%- set answer = utils.global_ns.var_stack.pop() -%}

  {%- if answer is true -%}
    {#- need to remember this path -#}
    {{ utils.getLastItemPath(globals) }}
    {%- set item = utils.global_ns.var_stack.pop() -%}
    {%- set local_ns.infrastructure_site_name_item_path = item -%}

    {{ utils.deepCopy(globals.global_ns.distribution_vars.infrastructure_site) }}
    {%- set site = utils.global_ns.var_stack.pop() -%}
    {%- do site.update({ "title" : reply }) -%}

    {%- do globals.global_ns.distribution_vars.infrastructure_sites.append(site) -%} 
  {%- endif -%}
{%- endmacro -%}


{%- macro processRdaInfrastructurePhysicalSiteURL(globals, reply) -%}
  {%- set create_new_site_object = false -%}
  {%- if not local_ns.infrastructure_site_name_item_path -%}
    {#- no site object has yet been created, -#}
    {#- because previous annotated question (site name) hasn't been answered -#}
    {%- set create_new_site_object = true -%}
  {%- else -%}
    {{ utils.checkIsSubItem(globals, local_ns.infrastructure_site_name_item_path, 1) }}
    {%- set answer = utils.global_ns.var_stack.pop() -%}

    {%- if answer is false -%}
      {%- set create_new_site_object = true -%}
    {%- endif -%}
  {%- endif -%}

  {%- if create_new_site_object is false -%}
    {%- set nb_sites = globals.global_ns.distribution_vars.infrastructure_sites|length -%}
    {%- set last_site = globals.global_ns.distribution_vars.infrastructure_sites[nb_sites - 1]  -%}
    {#- we can accept only one URL, the first documented -#} 
    {#- our model allows for more -#} 
    {%- if last_site.url -%}
      {#- reply is ignored -#}
    {%- else -%}
      {%- do last_site.update({ "url" : reply }) -%}
    {%- endif -%}
  {%- else -%}
    {#- in this case, it means that the site's name/title wasn't filled in, and no (new) site object -#}
    {#- therefore created: we'll create one, with an empty title of course -#}
    {{ utils.getLastItemPath(globals) }}
    {%- set item = utils.global_ns.var_stack.pop() -%}
    {%- set local_ns.infrastructure_site_name_item_path = item -%}

    {{ utils.deepCopy(globals.global_ns.distribution_vars.infrastructure_site) }}
    {%- set site = utils.global_ns.var_stack.pop() -%}
    {%- do site.update({ "url" : reply }) -%}

    {%- do globals.global_ns.distribution_vars.infrastructure_sites.append(site) -%} 
  {%- endif -%}
{%- endmacro -%}



