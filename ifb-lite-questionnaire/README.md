# IFB-lite Questionnaire Report

This is a customised version of the 
generic *default* template for [Data Stewardship Wizard](https://ds-wizard.org) (DSW), developed by DSW.


The  *default* template is available as open-source 
via the GitHub Repository [ds-wizard/questionnaire-report-template](https://github.com/ds-wizard/questionnaire-report-template). <br>

The IFB-lite template  is available as open-source 
via [GitLab Repository](https://gitlab.com/ifb-elixirfr/fair/gt2-is1-mudis4ls/output-templates/-/tree/main/ifb-lite-questionnaire?ref_type=heads).

Both templates are generic templates.

## Issues and Contributing

This document template for DSW is available as open-source via GitHub Repository [ds-wizard/questionnaire-report-template](https://github.com/ds-wizard/questionnaire-report-template), you can [report issues](https://github.com/ds-wizard/questionnaire-report-template/issues) there and fork it for customisations or contributions.

## Changelog


### 1.2.2



### 1.2.1

- update to metamodelVersion 16


### 1.2.0

- add an 'all_text' option
- and, re-engineered the whole code: now it looks like it has been
  written by a human!
  But be aware: the Mdoc version is not very practical to use...

### 1.1.10

- fix old bug with missing <div>
- update with new question type Item Select 
- but the whole lot should be properly rewritten; as it stands it is no more than a dirty hack

### 1.1.9

- update to metamodelVersion 14

### 1.1.8

- it seems I didn't publish properly...

### 1.1.7

- fixed the all questions, no answers output (docx)
- only answers output is ok (HTML, PDF, docx)

### 1.1.5, 1.1.6

- fixed the all questions, with or without output (HTML, PDF, docx)

### 1.1.4

- fixed the all questions, no answers output (HTML & PDF)


### 1.1.3

- update to metamodelVersion 13


### 1.1.2
    
- update to metamodelVersion 12

### 1.0.0

1rst version: without question.text, tags, question.extras

