## IFB Insight, listing annotations

A new way of getting insight in the KM: 
For every question, we list the 

- the question.title, the corresponding answers and annotations 
- together with links to [RDA DMP Common Standard (DCS)](https://github.com/RDA-DMP-Common/RDA-DMP-Common-Standard)

The template written for the IFB:pgd-structure:2.+ KM and used as 
programming support for the interop work.

## Issues and Contributing

This document template for DSW is available as open-source via the GitLab Repository [output_templates/ifb-insight](https://gitlab.com/ifb-elixirfr/fair/gt2-is1-mudis4ls/output-templates/-/tree/main/ifb-insight?ref_type=heads), you can [report issues](https://gitlab.com/ifb-elixirfr/fair/gt2-is1-mudis4ls/output-templates/-/issues) there and fork it for customisations or contributions.

### Contributors

* **Paulette Lieby** <[contact-dsw-if@groupes.france-bioinformatique.fr](mailto:contact-dsw-ifb@groupes.france-bioinformatique.fr)>
  * ORCID: [0000-0002-9289-9652](https://orcid.org/0000-0002-9289-9652)
  * GitLab: [@paule00](https://gitlab.com/paule00)




## Changelog
 
 
### 1.6.7
 
 
### 1.6.6

- metamodelVersion 16

### 1.6.5

- metamodelVersion 15
 
### 1.6.3

- update for new item select question

### 1.6.2

- added an archive option (tar) 
- metamodelVersion 14

### 1.6.1

- now can apply to all KMs

### 1.6.0

- update to new annotation engineering (with "interop" key)

### 1.5.3

- now a bare list of annotations is available
- fix README

### 1.5.2

- fix explanation

### 1.5.1

- added missing PDF documents so they exist for all types of output

### 1.5.0

- rda-dmp-cs tree is also collapsible (always open at start) 
  this makes it easier to read


### 1.4.0

- now with cardinalities as well

### 1.3.1

- small bug fix

### 1.3.0

- now, each document is shown together with, on the same page, the rda dmp tree,
  which itself refers back to the annotated DSW entities linking to it (a two-way linking
  of questionnaire annotations and rda dmp tree)


### 1.2.0

- complete re-engineering, get rid of duplicate code

### 1.1.2

- trying mapping with RDA-DMP-CS

### 1.1.1

fix all models following 1.1.0's modification in display


### 1.1.0

a new document model: list all questions with annotations and answers when they exist; as an accordeon

### 1.0.2

- metamodelVersion 13

### 1.0.1

- metamodelVersion 12

### 1.0.0

- Initial version
