#  awk -f rewrite_tree_as_j2.awk _rda-dmp-cs_original.html _rda-dmp-cs_tables_original.html > _rda-dmp-cs_tree.j2

# assert -- assert that a condition is true. Otherwise exit.
# Arnold Robbins, arnold@gnu.ai.mit.edu, Public Domain
# May, 1993
function assert(condition, string,    _assert_exit)
{
    if (! condition)
    {
        printf("%s:%d: assertion failed: %s\n",
               FILENAME, FNR, string) > "/dev/stderr";
        _assert_exit = 1;
        exit 1;
    }
}
END{
    if (_assert_exit)
    {
        exit 1;
    }
}


function update_table(table, max_i, key, cardinality,     _i)
{
    for (_i = 1; _i <= max_i; _i++)
    {
        if (table[_i, "item_id"] && table[_i, "item_id"] == key)
        {
            table[_i, "item_card"] = cardinality;

            break;
        }
    }
}

BEGIN{
    # external variables
    # -v uuid_file
    # -v map_file
    #
    # recall, default FS = " "
    i = 0;

    split("", table);
}
{
    if (FNR != NR)
        # we are reading the second file 
    {
        for (k = 1; k <= NF; k++)
        {
            if ($k ~ /href="\#/)
            {
                split($k, a, "\#");
                split(a[2], b, "\"");
                key = b[1];

                j = 0;
            }
        }

        if (j >= 0 && j < 4 && $1 ~ /<td/)
        {
            j++;
            if (j == 4)
            {
                split($2, a, ">");
                split(a[2], b, "<");
                update_table(table, max_i, key, b[1]);
            }
        }
    }
    else
    {
        i++;
        key = i;


        if ($1 ~ /<li/)
        {
            n = split($0, a, ">");

            split(a[1], b, "\"");
            table[key, "item_id"] = b[2];
        
            split(a[2], b, "\"");
            table[key, "item_href"] = b[2];

            split(a[3], b, "<");
            table[key, "item_text"] = b[1];
        }
        else
        {
            table[key, "section"] = $0;
        }
        max_i = i;
    }
}
END{
    print "{% set rda_html_tree = {";
    for (i = 1; i <= max_i; i++)
    {
        key_line = "l" i;
        if (table[i, "section"] || table[i, "item_id"])
        {
            print "'"key_line"' : {";

            if (table[i, "section"])
            {
                print "  'section' : '" table[i, "section"] "', ";
            }
            else
            {
                print " 'item_id' : '" table[i, "item_id"] "', ";
                print " 'item_href' : '" table[i, "item_href"] "', ";
                print " 'item_text' : '" table[i, "item_text"] "', ";
                print " 'item_card' : '" table[i, "item_card"] "', ";
            }
            print " }, ";
        }
    }
    print "} %}";
}
