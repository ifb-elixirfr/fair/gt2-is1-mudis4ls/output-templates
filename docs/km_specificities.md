
## <a name="DatasetTypes"></a>Dataset types in RDA DMP CS

Our knowledge model allows for datasets to have multiple types. This is not the case in RDA DMP CS.
The solution we adopt is to create as many datasets as there are types for one named dataset, 
and to name those created with the title `name ~ "-" ~ type`, 
so that each dataset has one type only. This implies that 
we create a map from a dataset name to a list of dataset titles.

In order to handle this in a transparent manner, we declare a few global variables and helper functions. 
Those are 


[`name_list`](../macros/#name_list)

[`type_list`](../macros/#type_list)

[`map_name_titles`](../macros/#map_name_titles)

### <a name="DatasetNameVsTitle"></a>Dataset name vs dataset title

Dataset title is the property of the [`dmp`](../rda_dmp_cs.json/#rda_dmp_cs).`dataset` object. Dataset names are
the names as gathered in the description of the datasets, while dataset titles are created from the names and 
types as explained [above](#DatasetTypes).

## <a name="DatasetItems"></a>Processing ItemList questions for datasets


As seen [here](../questionnaire_processing/#ListQuestions), specific processing is required when the answers 
to ItemList questions are complete. There are several situations to consider for datasets:

1. All answers to questions describing _one_ dataset: 
   macro [`formDatasetNamesAndClone`](../macros/#formDatasetNamesAndClone)

2. All answers to questions describing all datasets:
   macro [`doLastSetOfDatasetClones`](../macros/#doLastSetOfDatasetClones), called by 
   [`completeDatasetItemList`](../macros/#completeDatasetItemList).


## <a name="DatasetVersatility"></a>Versatility in dataset answers

Many questions in our knowledge model allow one to specify the dataset names to which the answers given will apply,
thus helping in reducing reduncancies. The generic macros below take care of this.


[`getConcernedDatasetNames`](../macros/#getConcernedDatasetNames)

[`getConcernedDatasetTitles`](../macros/#getConcernedDatasetTitles)



