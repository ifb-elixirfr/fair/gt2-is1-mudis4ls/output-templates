The `IFB Insight, showing annotations` has been written
as a debug support when developing the interoperability model [`RDA-DMP-CS DMP output; DMP-OPIDoR output`](../interop_with_annotations). It is a generic model.


## Caveat

* At the time of writing (July 2024),  we always show 
the mapping with 
RDA-DMP-CS: we need to rethink this bit, since we want to introduce the mapping
with DMP-OPIDoR's data dictionnary. _TDB_ then. 
* The RDA-DMP-CS is hard coded (in `jinja2`) and in this respect applies only  
  for the knowledge model for which it is written; otherwise the listing of annotations 
  applies to all knowledge models.

    
## Output options

For each option, a HTML or PDF output is available, unless specified otherwise.

### The list of annotations used

HTML only.

<img src="../assets/images/listing_all_annotations.png" style="height: 500px; width:800px;" />
 

### Only questions with annotations

Only questions having annotations are shown.

<img src="../assets/images/only_questions_with_annotations_showing_annotations.png" style="height: 500px; width:800px;" />

### Only questions having answers

Only questions having answers are shown, together with their answers    and their annotations if the latter exist.

<img src="../assets/images/only_questions_with_replies_showing_annotations_and_replies.png" style="height: 500px; width:800px;" />

### All questions are shown, without answers if they exist

All questions are shown, with their annotations if they exist, but without their answers when they exist.

<img src="../assets/images/all_questions_showing_annotations.png" style="height: 500px; width:800px;" />

### All questions are shown, together with their answers and their annotations

All questions are shown, together with their answers and their annotations  if they exist (can be collapsible or not).

<img src="../assets/images/all_questions_showing_annotations_and_replies.png" style="height: 500px; width:800px;" />
