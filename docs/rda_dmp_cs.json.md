
##  DMP in `_rda_template.j2`

We define <a name="rda_dmp_cs"></a>`dmp` as
`dmp = (_rda_template.j2).dmp_skeleton`.

```
{%- set dmp_skeleton = {
  "contact": {},
  "contributor": [],
  "dataset": [],
}
-%}
```

### Attributes `dmp.contributor, dmp.contact`

```
{%- set contact = {
  "name": none,
  "mbox": none,
  "contact_id": {}
}
-%}

{%- set contact_id = {
  "identifier": none,
  "type": none
}
-%}

{%- set contributor = {
  "name": none,
  "mbox": none,
  "role": [],
  "contributor_id": {}
}
-%}

{%- set role = none
-%}

{%- set contributor_id = {
  "identifier": none,
  "type": none
}
-%}
```

### Attribute `dmp.dataset`
#### Properties `title`, `type`¸ `personal_data`, `sensitive_data`
```
{%- set dataset = {
  "data_quality_assurance": [],
  "dataset_id": {},
  "description": none,
  "distribution": {},
  "issued": none,
  "keyword": [],
  "language": none,
  "metadata": [],
  "personal_data": none,
  "preservation_statement": none,
  "security_and_privacy": [],
  "sensitive_data": none,
  "technical_resource": [],
  "title": none,
  "type": none,
}
-%}
```

#### Properties `description`, `dataset_id`

#### In `_rda_template.j2`
```
{%- set dataset_id = {
  "identifier": none,
  "type": none
}
-%}
```
