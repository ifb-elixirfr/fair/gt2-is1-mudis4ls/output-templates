

# DSW document models to implement interoperability

## Description

This repository hosts DSW document models being developed, among other things,
to 
implement DSW interoperability with DMP-OPIDoR. More precisely, they
implement the interoperability of the questionnaires based on the 
knowledge model developed within the IFB MUDI4LS project; this is described
[here](https://ifb-elixirfr.gitlab.io/fair/dsw-ifb-instance/).

Interoperability implementation is based on the use of [annotations](https://guide.ds-wizard.org/en/latest/about/introduction/knowledge-model.html#annotations) within the DSW knowledge model. 
For more on the vocabulary used within the DSW context, you may visit 
[this tutorial](https://docs.google.com/presentation/d/e/2PACX-1vS54Ckm-4156SetlEjS_CqeHce8oDGIUMxVmq_tZbJVKbBljqrISUoE4zs1YUnbT362Lufi_2a47sBs/pub?start=false&loop=false&delayms=3000#slide=id.g2ca876c39e5_0_198).


## In these pages

You'll find

1. A presentation and discussion around annotations
2. A somewhat detailed description of the output templates developed so far
3. Coding notes

## Roadmap

1. Creating a document model implementing interoperability using the RDA DMP CS [exchange format](https://github.com/RDA-DMP-Common/RDA-DMP-Common-Standard). 

    A 1rst version (RDA-DMP-CS DMP output; DMP-OPIDoR output V1.0.0) 
    of this model is available as a proof of concept since May 2024.
    At the time of writing (July 2024) there are still a few bugs needing to
    be sorted out.
    


2. Creating a document model implementing interoperability using the DMP-OPIDoR [data dictionnary](https://opidor.github.io/maDMP-OPIDoR_documentation/).

    We are currently working on a use case with DMP-OPIDoR and Oana Vigy
    from Biocampus, Montpellier.

## Authors and acknowledgment
Paulette Lieby.

## Developers
Paulette Lieby, Sylvain Milanesi.

## License
GNU GPLv3 

## Project status
Work in progress. 
