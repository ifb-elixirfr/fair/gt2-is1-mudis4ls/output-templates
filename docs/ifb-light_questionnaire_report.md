
The `IFB-lite Questionnaire Report` is a slight rewrite of DSW's default generic document model `Questionnaire Report`; it is thus a generic model. 
It is a rewrite in the sense that the output document is much shorter, mostly because
we don't show the explanatory text accompagnying the question.


## Output options

For each option, a HTML, MS Word, or PDF output is available.

1. Only questions having answers are shown: useful to have a concise version of the questionnaire.
2. All questions are shown, but without their answers when they exist: useful to read a 
   knowledge model in full.
3. All questions are shown, together with their answers when they exist.
