
| attribute | data type | cardinality | comments | 
| --- | --- | --- | --- |
`dmp.contributor`  |  nested data structure  |  0..n | |
`dmp.contributor.mbox`  |            string |  0..1 | |
`dmp.contributor.name`  |           string  |  1 | |
`dmp.contributor.role`  |              string | 1..n | |
`dmp.contributor.contributor_id`  |     nested data structure  |  1 | |
`dmp.contact.contributor_id.identifier` |   string  |  1 | |
`dmp.contact.contributor_id.type` |       term from controlled vocabulary  |   1  | values allowed: orcid, isni, openid, other |
| | | |
`dmp.contact`   |                       nested data structure   |  1 | |
`dmp.contact.name` |                    string  | 1 | |
`dmp.contact.mbox`  |                    string  | 1 | |
`dmp.contact.contact_id`  |             nested data structure  |  1 | |
`dmp.contact.contact_id.identifier` |    string  |  1 | |
`dmp.contact.contact_id.type` |       term from controlled vocabulary  |       1 | values allowed: orcid, isni, openid, other |
| | | |
`dmp.dataset`  |  nested data structure | 1..n | |
`dmp.dataset.description` |            string | 0..1 | |
`dmp.dataset.dataset_id` |             nested data structure | 1  |  | 
`dmp.dataset.dataset_id.identifier`   |      string | 1  |  |
`dmp.dataset.dataset_id.type` | term from controlled vocabulary  | 1 |      values allowed: handle, doi, ark, url, other | |
`dmp.dataset.personal_data` | term from controlled vocabulary | 1 | yes, no, unknown |
`dmp.dataset.sensitive_data` | term from controlled vocabulary  | 1 | yes, no, unknown |
`dmp.dataset.title`  |   string | 1 | |    
`dmp.dataset.type`  | string  |  0..1  | |  

