
Annotations as they appear when processed, i.e. in chronological order.


| annotation (key) |
| ------ | 
| "Infrastructure" |
| "InfrastructureName" |
| "InfrastructureAcronym" |
| "InfrastructureURL" |
| "InfrastructurePhysicalSite" |
| "InfrastructurePhysicalSiteName" |
| "InfrastructurePhysicalSiteURL" |
| "Contributor" |
| "ContributorName" |
| "ContributorMbox" |
| "ContributorId" |
| "ContributorIdType" |
| "ContributorIdIdentifier" |
| "ContributorRole" |
| "Contact" |
| "ContributorIdROR" |
| "ContributorContributorId" |
| "ContributorIdType" |
| "ContributorIdIdentifier" |
| "Dataset" |
| "DatasetName" |
| "DatasetDescription" |
| "DatasetType" |
| "DatasetId" |
| "DatasetIdIdentifier" |
| "DatasetIdType" |
| "DataCollectionNames" |
| "DatasetDistributionFormat" |
| "DataDocumentationNames" |
| "DistributionByteSizeKB" |
| "DistributionByteSizeMB" |
| "DistributionByteSizeGB" |
| "DistributionByteSizeTB" |
| "DistributionByteSizeValue" |
| "DistributionOtherUnitSize" |
| "DataSecurityNames" |
| "DistributionAccessURL" |
| "DataStorageSharingNames" |
| "DatasetPersonalData" |
| "DatasetSensitiveData" |
