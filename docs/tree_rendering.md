## <a name="Tree"></a>Tree rendering


The idea here is to give a tree representation of 
the annotated project, i.e. the questionnaire based on the 
annotated knowledge model.

### Example

This is the annotated tree rendering of a sample [project](../assets/documents/example_PGD_structure_développé_par_lIFB.pdf).

```
  {
    "tree": {
        "Contributor": [
            {
                "ContributorId": [
                    {
                        "ContributorIdIdentifier": "0000-0002-9289-9652",
                        "ContributorIdType": [
                            "ORCID",
                            {
                                "ContributorIdType": null
                            }
                        ]
                    },
                    {
                        "ContributorIdIdentifier": "identifiant",
                        "ContributorIdType": [
                            "Autre",
                            {
                                "ContributorIdType": "un autre type d\u0027identifiant"
                            }
                        ]
                    },
                    {
                        "ContributorIdIdentifier": null,
                        "ContributorIdType": [
                            "Autre",
                            {
                                "ContributorIdType": null
                            }
                        ]
                    },
                    {
                        "ContributorIdIdentifier": null,
                        "ContributorIdType": [
                            {
                                "ContributorIdType": null
                            }
                        ]
                    }
                ],
                "ContributorIdROR": [],
                "ContributorMbox": [
                    "paulette.lieby@france-bioinformatique.fr"
                ],
                "ContributorName": "Lieby",
                "ContributorRole": [
                    {
                        "Contact": "Personne contact"
                    },
                    "Responsable du plan de gestion de donn\u00e9es",
                    "Gestionnaire de donn\u00e9es"
                ]
            },
            {
                "ContributorId": [
                    {
                        "ContributorIdIdentifier": "truc muche",
                        "ContributorIdType": [
                            "HAL",
                            {
                                "ContributorIdType": null
                            }
                        ]
                    },
                    {
                        "ContributorIdIdentifier": null,
                        "ContributorIdType": [
                            {
                                "ContributorIdType": null
                            }
                        ]
                    }
                ],
                "ContributorIdROR": [],
                "ContributorMbox": [
                    "paulette.lieby@france-bioinformatique.fr"
                ],
                "ContributorName": "lolo",
                "ContributorRole": [
                    {
                        "Contact": null
                    },
                    "Responsable de la conservation des donn\u00e9es \u00e0 long terme",
                    "Responsable juridique"
                ]
            },
            {
                "ContributorId": [
                    {
                        "ContributorIdIdentifier": null,
                        "ContributorIdType": [
                            {
                                "ContributorIdType": null
                            }
                        ]
                    },
                    {
                        "ContributorIdIdentifier": "blabal",
                        "ContributorIdType": [
                            "Autre",
                            {
                                "ContributorIdType": "blabla"
                            }
                        ]
                    }
                ],
                "ContributorIdROR": [
                    "https://ror.org/02feahw73"
                ],
                "ContributorMbox": [],
                "ContributorName": null,
                "ContributorRole": [
                    {
                        "Contact": null
                    },
                    "Entrep\u00f4t h\u00e9bergement des donn\u00e9es",
                    "Autre",
                    "some role"
                ]
            }
        ],
        "DataCollectionNames": [],
        "DataDocumentationNames": [],
        "DataSecurityNames": [],
        "DataStorageSharingNames": [],
        "Dataset": [
            {
                "DatasetDescription": "un jeu de donn\u00e9es A",
                "DatasetId": [
                    {
                        "DatasetIdIdentifier": "A",
                        "DatasetIdType": [
                            "ARK"
                        ]
                    },
                    {
                        "DatasetIdIdentifier": "B",
                        "DatasetIdType": [
                            "DOI"
                        ]
                    }
                ],
                "DatasetName": "A",
                "DatasetType": null
            },
            {
                "DatasetDescription": "un jeu de donn\u00e9es B",
                "DatasetId": [
                    {
                        "DatasetIdIdentifier": null,
                        "DatasetIdType": []
                    }
                ],
                "DatasetName": "B",
                "DatasetType": null
            },
            {
                "DatasetDescription": "un jeu de donn\u00e9es C",
                "DatasetId": [
                    {
                        "DatasetIdIdentifier": null,
                        "DatasetIdType": []
                    }
                ],
                "DatasetName": "C",
                "DatasetType": null
            },
            {
                "DatasetDescription": null,
                "DatasetId": [
                    {
                        "DatasetIdIdentifier": null,
                        "DatasetIdType": []
                    }
                ],
                "DatasetName": null,
                "DatasetType": null
            }
        ],
        "DatasetDistributionFormat": [],
        "DatasetPersonalData": [
            "Non"
        ],
        "DatasetSensitiveData": [
            "Non"
        ],
        "DistributionAccessURL": [],
        "DistributionByteSizeGB": [],
        "DistributionByteSizeKB": [],
        "DistributionByteSizeMB": [],
        "DistributionByteSizeTB": [],
        "DistributionByteSizeValue": [],
        "DistributionOtherUnitSize": [],
        "Infrastructure": [
            {
                "InfrastructureAcronym": "truc",
                "InfrastructureName": "structure-truc-muche",
                "InfrastructurePhysicalSite": [
                    {
                        "InfrastructurePhysicalSiteName": "A",
                        "InfrastructurePhysicalSiteURL": [
                            "https://machin.chose",
                            "https://autre.machin.chose"
                        ]
                    },
                    {
                        "InfrastructurePhysicalSiteName": "B",
                        "InfrastructurePhysicalSiteURL": []
                    },
                    {
                        "InfrastructurePhysicalSiteName": null,
                        "InfrastructurePhysicalSiteURL": [
                            "https://truc.muche"
                        ]
                    }
                ],
                "InfrastructureURL": [
                    "https://www.plieby.com",
                    "https://www.principale.com"
                ]
            }
        ]
    }
}
```



### What's next?

TBD




