
## Some global variables

### In all files

We define <a name="dmp_being_built">`dmp_being_built`</a> 
as the dmp being built using RDA DMP CS and 
[`dmp`](../rda_dmp_cs.json/#rda_dmp_cs) as
`dmp = (_rda_template.j2).dmp_skeleton`.

### In `_global_variables.j2`

#### Items in `global_ns.dataset_vars`

<a name="name_list"></a>`name_list`

Description: collects all dataset names for the whole questionnaire. See [here](../km_specificities/#DatasetTypes).

<a name="type_list"></a>`type_list`

Description: collects all types for a given dataset name. See [here](../km_specificities/#DatasetTypes).

<a name="map_name_titles"></a>`map_name_titles`

Description: maps a dataset name to a list of dataset titles. See [here](../km_specificities/#DatasetTypes).



### In `_monitoring`

<a name="annotations_encountered_stack"></a>`annotations_encountered_stack`

Description: a stack of lists: each list collects the
action annotations encountered while processing an ItemList (thus, one list for each ItemList). See [here](../dsw_specificities/#ListQuestions).


## Utility macros

### In `_utils.j2`

<a name="deepCopy"></a>`deepCopy`

Description: makes a deep copy of a json object; this is required as `jinja2` accesses vars by reference only: a statement like `set var = object` does not create a copy of `object` but only a link to it.

<a name="checkEntityPartOfObject"></a>`checkEntityPartOfObject`

Description: tests if a (uuid) path  is a superset of another path  objectPath; 
to put it differently, is objectPath a prefix of path? <br> 
This is relevant when seeking to ascertain that a given answer refers to a particular object. <br>
A case in point are objects that are ItemList questions: 
to which item of a ItemList does an answer belong to? (refer to <a href="../dsw_specificities/#ListQuestions" target="_blank">this</a> discussion).

<a name="getConcernedDatasetNames"></a>`getConcernedDatasetNames`

Description: Read dataset names from a given answer and return them if they
exist in [`name_list`](#name_list).

Description: For a given dataset name, return 

<a name="getConcernedDatasetTitles"></a>`getConcernedDatasetTitles`

Description: return all datasets mapped to the titles in a list of names. 
That is, for all names in the list, return the union of the dataset titles in 
[`map_name_titles`](#map_name_titles)\[index of name in [`name_list`](#name_list)\]; see 
[here](../km_specificities/#DatasetNameVsTitle).

### In `_monitoring.j2`
 
<a name="trackItemListStart"></a>`trackItemListStart`

Description: macro called before an ItemList is processed; it appends a 
new list to [`annotations_encountered_stack`](#annotations_encountered_stack); all annotations encountered 
thereafter will be appended to this list.


<a name="processItemListEnd"></a>`processItemListEnd`

Description: macro called once an ItemList has been processed; 
this allows for specific processing to take place if required.
For see [`completeDatasetItemList`](#completeDatasetItemList) below.


<a name="completeDatasetItemList"></a>`completeDatasetItemList`

Description: call [`doLastSetOfDatasetClones`](#doLastSetOfDatasetClones) once all answers have been 
processed. See [here](../km_specificities/#DatasetItems).


### In `_dataset.j2`

<a name="formDatasetNamesAndClone"></a>`formDatasetNamesAndClone`
```
do some cardinality checks on dmp_being_built.dataset[last_element]
local_title_list = []
get the last name added to name_list
for each type in type_list
  dataset_title = name ~ "-" ~ type
  append dataset_title to local_title_list
  if type == type_list[0]
    update title of dmp_being_built.dataset[last_element] with dataset_title
    update type of dmp_being_built.dataset[last_element] with type
  else
    obj = utils.deepCopy of dmp_being_built.dataset[last_element]
    update obj with dataset_title
    update obj with type
    append obj to dmp_being_built.dataset
append local_title_list to map_name_titles
```

<a name="doLastSetOfDatasetClones"></a>`doLastSetOfDatasetClones`

Description: call  [`formDatasetNamesAndClone`](#formDatasetNamesAndClone) once all 
dataset names have been collected for the questionnaire; 
this will be called through [`processItemListEnd`](#processItemListEnd), see also 
this [discussion](../dsw_specificities/#ListQuestions).




## Annotation processing

### In `_contributor_contact.j2`


<a name="RDAContributor"></a>RDAContributor
```
obj = utils.deepCopy of dmp.contributor
append obj to dmp_being_built.contributor
```


<a name="RDAContributorName"></a>RDAContributorName  (organisation only)
```
dmp_being_built.contributor[last_element].name = RDAContributorName
```



<a name="rdaContributorLastname"></a>rdaContributorLastname (person only)
```
dmp_being_built.contributor[last_element].name = rdaContributorLastname
```


<a name="rdaContributorFirstname"></a>rdaContributorFirstname (person only)
```
if str = dmp_being_built.contributor[last_element].name
  dmp_being_built.contributor[last_element].name = rdaContributorFirstname ~ " " ~ str
else
  dmp_being_built.contributor[last_element].name = rdaContributorFirstname
```

<a name="RDAContributorMbox"></a>RDAContributorMbox
```
dmp_being_built.contributor[last_element].mbox = RDAContributorMbox
```

<a name="rdaContributorIdROR"></a>rdaContributorIdROR  (organisation only)
```
obj = utils.deepCopy of dmp.contributor_id
dmp_being_built.contributor[last_element].contributor_id = obj
dmp_being_built.contributor.contributor_id.type = "ror"
dmp_being_built.contributor.contributor_id.identifier = rdaContributorIdROR
```

<a name="RDAContributorContributorId"></a>RDAContributorContributorId
```
if not dmp_being_built.contributor[last_element].contributor_id is empty (ie an empty dict {})
  obj = utils.deepCopy of dmp.contributor_id
  dmp_being_built.contributor[last_element].contributor_id = obj
  
  contributor_id_item_path = path
else
  contributor_id_item_path = null
```

<a name="RDAContributorIdType"></a>RDAContributorIdType
```
if contributor_id_item_path is non null
  if utils.checkEntityPartOfObject
    dmp_being_built.contributor.contributor_id.type = RDAContributorIdType
```

<a name="RDAContributorIdentifierType"></a>RDAContributorIdentifierType
```
if contributor_id_item_path is non null
  if utils.checkEntityPartOfObject
    dmp_being_built.contributor.contributor_id.identifier = RDAContributorIdIdentifier
```

<a name="RDAContributorRole"></a>RDAContributorRole
```
append RDAContributorRole to dmp_being_built.contributor.role
```

<a name="RDAContact"></a>RDAContact
```
if dmp_being_built.contact\|length == 0
  obj = utils.deepCopy of dmp.contact
  dmp_being_built.contact = obj
  dmp_being_built.contact.name = dmp_being_built.contributor[last_element].name
  dmp_being_built.contact.mbox = dmp_being_built.contributor[last_element].mbox
  obj = utils.deepCopy of dmp_being_built.contributor[last_element].contributor_id
  dmp_being_built.contact.contact_id = obj
```
    

### In `_dataset.j2`

<a name="RDADataset"></a>RDADataset
```
if dmp_being_built.dataset not empty
  formDatasetNamesAndClone
  type_list = []
  obj = utils.deepCopy of dmp.dataset
  append obj to dmp_being_built.dataset
```


<a name="rdaDatasetName"></a>rdaDatasetName
```
append name to the global list name_list
```

<a name="RDADatasetDescription"></a>RDADatasetDescription
```
update dmp_being_built.dataset[last_element].description with RDADatasetDescription
```


<a name="RDADatasetType"></a>RDADatasetType
```
append name to the global list type_list
```

<a name="RDADatasetDatasetId"></a>RDADatasetDatasetId
```
if not dmp_being_built.dataset[last_element].dataset_id is empty (ie an empty dict {})
  obj = utils.deepCopy of dmp.dataset_id
  dmp_being_built.dataset[last_element].dataset_id = obj
  
  dataset_id_item_path = path
else
  dataset_id_item_path = nul
```
  
<a name="RDADatasetIdType"></a>RDADatasetIdType
```
if dataset_id_item_path is non null
  if utils.checkEntityPartOfObject
    dmp_being_built.dataset[last_element].dataset_id.type = RDADatasetIdType
```
    
<a name="RDADatasetIdIdentifier"></a>RDADatasetIdIdentifier
```
if dataset_id_item_path is non null
  if utils.checkEntityPartOfObject
    dmp_being_built.dataset[last_element].dataset_id.identifier = RDADatasetIdIdentifier
```







### In `_elsi.j2`

<a name="datasetELSINames"></a>datasetELSINames

```
gather all dataset names concerned 
```

<a name="RDADatasetPersonalData"></a>RDADatasetPersonalData
```
update dmp_being_built.dataset.personal_data for all datasets concerned 
```


<a name="RDADatasetSensitiveData"></a>RDADatasetSensitiveData
```
update dmp_being_built.dataset.sensitive_data for all datasets concerned 
```
