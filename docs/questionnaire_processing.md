## <a name="RetrievingAnswers"></a> Retrieving answers


In DSW, each entity (see [DSW's semantics](https://guide.ds-wizard.org/en/latest/_images/document-context.svg)) is identified by its `uuid`. 
The `uuid` path to an answer is a concatenation of the `uuid`s of each entity on the
path to an answer, remembering that a DSW questionnaire is a tree of questions 
and subquestions.

### `uuid` path

One would think that the question's `uuid` is sufficient to identify its answer, 
but this is not true for List of Items questions, see [below](#ListOfItemsQuestion).
The `uuid` path is required as a single question can appear multiple times 
as items in this type of question  duplicate the subtree (of questions)
possibly infinitely.

Thus, answers in the DSW questionnaire are identified by their `uuid` path.

### <a name="ListOfItemsQuestion"></a> Dealing with List of Items questions

To quote DSW, a DSW question ot type [List of Items](https://guide.ds-wizard.org/en/latest/application/projects/list/detail/questionnaire.html#list-of-items-question) is 


> List of Items question doesn’t have a simple answer but a list of items. Each of the items has the same set of subquestions. For example, a list of items question asking about the project contributors where each item represent one contributor with questions about their name, role, etc.

For short, we call such a question an ItemList question. An item, or block of 
subquestions, may be repeated as many times as necessary
when answering this type of question.

From an annotation and answer processing perspective, 
it is necessary to track the items and their corresponding answers: yes, answers
are implicitely understood to "belong" to the proper item (through their 
`uuid`path) but if an item needs to be identified (either as a `json` object or 
property, or a `jinja2` variable), answers need to be matched to the proper item. 
We do this using annotations and the `uuid` path. 

#### An example



As an example, assume that we have the following answers in the questionnaire

```
ItemList question   
  ItemA
    question x  
        answer x
    question y
  ItemB
    question x
    question y
        answer y
```

#### Annotate ItemList questions 

We annotate `ItemList question`, 
say with `ItemListAnnotation`. We cannot annotate items
as annotations are created in the knowledge model, at which point no item 
yet exists (but the item's subquestions do exist).
We also annotate `question y` with `QuestionYAnnotation` to give
```
ItemList question     ItemListAnnotation
  ItemA
    question x  
        answer x
    question y        QuestionYAnnotation
  ItemB
    question x
    question y
        answer y      QuestionYAnnotation
```
Note that not all ItemList questions need to be annotated, only those 
necessary to identify either a `json` object, or a `jinja2` variable. 


#### Recording `uuid` paths

When processing the questionnaire, 
we must encounter 
`ItemListAnnotation` since there are answers to `ItemList question`.
These answers come in the form of the creation of  one or more items (here two), which
are again identified by their respective `uuid`.

Upon encountering `ItemListAnnotation`, it is now enough, for each item
created, to record its `uuid` path. Upon encountering `QuestionYAnnotation` 
(for `ItemB` only, since there is no answer to that question in `ItemA`) we also record its `uuid` path. 

Finally, to determine if `answer y` concerns `ItemA` or `ItemB`, it is 
enough to check which `uuid` path of `ItemA` or `ItemB` is a subpath
of `answer y`'s `uuid` path.


#### <a name="ChildParentRelation"></a> Caveat

1. Actually, in the code itself, we are proceeding a little bit differently,
   but in an equivalent manner: as we 
   process each entity in the questionnaire, we are building (on the fly) the set of 
   `[child, parent]` relations. Then at the point of processing `answer y` we simply
   check if `answer y`'s `uuid` is a child of `ItemA` or `ItemB`'s `uuid` which
   has been previously recorded.

2. Annotating ItemList questions allows associating item answers 
   with a `json` object or property, or a `jinja2` variable. When those
   are created on the fly, we may assume
   that items are associated with the latest created `json` 
   object or `jinja2` variable. 
   
    In this respect we could then consider that `uuid` path recording and 
   subpath checking  is strictly speaking not required. This is true, 
   but _only in this specific case_; here could
   we consider this checking as a sanity check. 




## Overview of annotation processing 

* What we mean by annotation processing here, is how to use the presence of annotations and answers in the 
  DMP to construct the output [`json` object](../interop_with_annotations/#the-output-json-object).
* The way annotations are processed depends on the question type.
In DSW, questions are of several types; we won't review them here, see [this page](https://guide.ds-wizard.org/en/latest/about/introduction/knowledge-model.html#question) in the DSW manual for further information. The quotes below are from this manual.
* Recall that we only process those questions having answers.
* Note also that in our case we assume that among DSW entities, only questions, options (in Options
  questions), and choices (in Multi-choice questions) may be annotated. We ignore all other entities.


### List of Items question

> The list of items question is used when there are multiple answers and we want to ask more details about those. 

See also the [above section](#dealing-with-list-of-items-questions).

#### Processing

```
if List of Items question is annotated
    process question annotation using no answer
    for each item in answer
       for each subquestion in item
           process subquestion   
```

#### Comments

* `using no answer`: a List of Items question has no direct answer, only the subquestions 
  (or follow-up questions) in the items comprising the answer may have direct answers
* `process subquestion`: this refers to the higher up question processing


### Multi-Choice Question

> The multi-choice question has a list of choices. Users can then pick as many of those choices as they wish. There are, however, no follow-up questions available for this question type.


#### Processing

```
for choice in answer 
    if Multi-Choice question is annotated
        process question annotation using choice as an answer
    if choice is annotated 
        process choice annotation using choice as an answer
```

#### Comments

* both the question annotation and the choice annotation are processed, using the _same_ answer,
  the choice label
* this may sound strange, but it is the responsibility of the person annotating the knowledge model
  to ensure there are no conflicts: typically one would not annotate the question and its choices
  with the same annotation
* this way of doing allows one to either annotate the question, or the choice itself: the background 
  processing will then handle this smoothly
  


### Options question

> The options question contains a closed list of answers where users can pick one. Answers can have some follow-up questions that are only presented to the user when they pick the answer. 


#### Processing

```
if Options question is annotated
    process question annotation using option chosen as an answer
if option chosen is annotated 
    process option annotation using option chosen as an answer 
for each follow-up question of the chosen option
    process follow-up question
```


#### Comments

* with respect to processing both the question and the option using the same answer,
  the same comments as in [Multi-Choice Question](#multi-choice-question) apply
* `process follow-up question`: this refers to the higher up question processing


### Value and Integration question 

> The value question asks for a single value that users type in. 
> The integration question is connected to an external resource where the users can pick the answer from.

Both types of questions are processed in the same way with respect to annotation processing.

#### Processing

```
if question is annotated
    process question annotation using the answer 
```

#### Comments

* this is the simplest case: processing the question annotation using the answer given


### Final comment

Of course we did not describe here what "annotation processing" means here. It may consist of creating
`json` objects or `jinja2` variables, and/or of updating them.
The challenge is precisely to be able to formalise this, this is WIP (or rather thinking in progress!!!).
Today actual processing is buried in the code itself...


## Constructing the annotation tree

<span style="color:Red"><b>Work In Progress</b></span>

To make the description here easier, we assume that we only process
questions with answers. In actual fact however, we do create nodes in the tree for all annotated 
entities (with or without answers); see [here](../interop_with_annotations/#the-annotation-tree).

We should actually create two different outputs for clarity (see also [here](interop_with_annotations/#caveat))

1. the annotation tree representing the filled in questionnaire
2. the annotation tree representing an empty questionnaire, thus listing all annotations, irrespective 
   of the existence of answers

We start with
```
set node_stack = {}
```

### List of Items question


### Multi-Choice Question

### Options question


### Value and Integration question 

## Constructing the list of `[child, parent]` relations

We only process those questions having answers.


We define
```
function updateList(list_of_relations, parent, child)
    set child = not found
    for key, value in list_of_relations
        if key == child
            value.append(parent)
            set child = found
    if child not found
        list_of_relations.append({child, [parent]})
```


We start with
```
uuid_stack.push(uuid_root)
list_of_relations = []
updateList(list_of_relations, uuid_stack.last, questionnaire.uuid)
uuid_stack.append(questionnaire.uuid)
for chapter in questionnaire.chapters
    process(chapter)
uuid_stack.pop()
```

### Chapter

```
function process_chapter(chapter)
    updateList(list_of_relations, uuid_stack.last, chapter.uuid)
    uuid_stack.append(chapter.uuid)
    for question in chapter.questions
        process_question(question)
    uuid_stack.pop()
```

### Question

```
function process_question(question)
    if question.answer exists
        updateList(list_of_relations, uuid_stack.last, question.uuid)
        uuid_stack.append(chapter.uuid)
        if question is of type 'List of Items'
            process_list-of-items(question)
        else if question is of type 'Multi-Choice'
            process_multi-choice(question)
        else if question is of type 'Options'
            process_options(question)
        else 
            process_value(question)
        uuid_stack.pop()
```



### List of Items question
```
function process_list-of-items(question)
    for each item in question.answer
        updateList(list_of_relations, uuid_stack.last, item.uuid)
        uuid_stack.append(item.uuid)
        for question in item.questions
            process_question(question)
        uuid_stack.pop()
```


### Multi-Choice Question

```
function process_multi-choice(question)
    for choice in question.answer 
        updateList(list_of_relations, uuid_stack.last, choice.uuid)
        uuid_stack.append(choice.uuid)
        uuid_stack.pop()
```


### Options question

```
function process_options(question)
    updateList(list_of_relations, uuid_stack.last, option.uuid)
    uuid_stack.append(option.uuid)
    for question in option.questions
        process_question(question)
    uuid_stack.pop()
```




### Value and Integration question 

There is nothing to do for this type of question with respect to `[child, parent]` 
relations: this type of question is a leaf of the questionnaire tree.


## Final comment
