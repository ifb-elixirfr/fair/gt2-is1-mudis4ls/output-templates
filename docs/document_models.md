


## Generic vs specialised document models

A generic model may be applied to questionnaires/projects based 
on *_any_* DSW knowledge model. This is opposed to specialised models
which apply to questionnaires based of *_specific_*  knowledge models: that is,
specialised document models are written for a specific knowledge model.


## The document models developed by ourselves


At the time of writing (June 2024), there are three models we develop:

1. [`IFB-lite Questionnaire Report`](../ifb-light_questionnaire_report)

    This is a slight rewrite of DSW's default generic document model `Questionnaire Report`; it is thus a generic model. It is a rewrite in the sense that the output document is much shorter.

2. [`IFB Insight, showing annotations`](../insight_annotations)

    This is also a generic model, written as a tool to help the development of the interoperability model (the one below). Its main function is to list all annotations.
    
3. [`RDA-DMP-CS DMP output; DMP-OPIDoR output`](../interop_with_annotations)

    This a specific model, written for the knowledge model developed within the IFB MUDI4LS project
    (described
[here](https://ifb-elixirfr.gitlab.io/fair/dsw-ifb-instance/)).


