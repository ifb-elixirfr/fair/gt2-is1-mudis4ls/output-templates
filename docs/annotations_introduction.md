



Here we'll describe some rules governing our use of annotations in DSW. Their
purpose is implementing the interoperability of 
the questionnaires based on the knowledge model developed 
within the IFB MUDI4LS project.

It is important to note that this is work in progress and that for the 
time being some of the rules are
revised as progress is being made. 

## Preliminary remarks about annotations in DSW

- All DSW entities (questions, options, choices, phases, etc...) may be annotated; it may happen that a question, say an Option question has the same annotation as one of its option answer. This is also true for the Multichoice questions or the ItemList questions. It is the annotator's responsibility to handle this in a coherent manner.
- A DSW entity may have more than one annotation. This means that for one given entity, there may exist an annotation whose purpose is interoperability with the RDA DMP CS exchange format or with the DMP-OPIDoR data dictionnary, and another whose purpose is the creation of an RDF output.
- A DSW entity is annotated using a python dictionnary, i.e. a set of `key-value` pairs. Each pair defines an annotation.
- When processing annotations when writing the `jinja2` templates, it is useful to remember that annotations appear in _chronological_ order, that is, as they appear when expanding the questionnaire. Most `jinja2` processing implicitely relies on this fact.


## Some decisions we made

- For the annotations deployed for the purpose of interoperability, we use the key value of `interop`
    - To find the desired annotation
    
``` 
if question.annotations
  for key, annotation in question.annotations
    if key == "interop"
```
&ensp; &ensp; &ensp; &ensp; &ensp; &ensp; If the purpose here is to access the answer to `question` then this acces is given by
    
``` 
    reply = question.replies.get(path)
```
&ensp; &ensp; &ensp; &ensp; &ensp; &ensp; where `path` is the concatenation of all uuids traversed to reach `question`.

- To implement the annotations to achieve interoperability with RDA DMP CS, one could use [dcso](https://github.com/RDA-DMP-Common/RDA-DMP-Common-Standard/blob/master/ontologies/core/dcso.4.0.0.ttl), the RDA DMP CS ontology.
  We decided not use dcso as it would require time and people resources 
  which we currently do not have.

## General rules about our use of annotations

- **Rule 1**. All DSW entities may be annotated: chapters, all types of questions, option answers, multi choices, tags, phases, and metrics. 
- **Rule 2**. A DSW entity may be annotated with more than one annotation: one may refer to the mapping to the RDA DMP CS exchange format or the DMP-OPIDoR data dictonnary, another may refer to a RDF representation. 
- **Rule 3**. Several DSW entities may share the same annotation: that is, it is the responsibility 
  of the developer to handle those situations should they arise. _Note_: this might change in the future.
- **Rule 4**. <a name="rule-key"></a> Annotations whose purpose is interoperability, that is, 
  they implicitely  describe a mapping to RDA-DMP-CS or DMP-OPIDoR,
  are `key-value`
  pairs where the key value is `interop`.
- **Rule 5**. At the time of writing (June 2024), the 
  same interoperability 
  annotation  may both refer to a  RDA-DMP-CS mapping or a DMP-OPIDoR
  mapping. It is hoped that the distinction can be made exclusively
  at the (`jinja2`) code level. _Note_: this too might change in the future.
          
## General rules about our processing of the questionnaire

- **Rule 1**. _Annotations are processed only for those questions having been answered_. This implies that for any given questionnaire, not all annotations may be seen.
- **Rule 2**. When processing the questionnaire, we make _no assumption on 
  the existence of prior answers_. To clarify: 
    - For example, assume that we ask for a contributor's first name 
      and last name, in that order
    - Suppose the required output is a name composed of 
      `first name + last name`:
         - we cannot assume that an answer to the `first name` question exists
           and must compose the desired output `first name + last name`
           with this in mind
    - Another example is to consider when to create a new contributor `json`
      object (since we are creating a DMP as a `json` object): more than one
      contributor may be described.
        - we cannot assume that the answer to a specific question 
          (say `last name`) triggers the creation of the new contributor
        - we need to handle this differently, typically using annotations 
          and the DSW structuration of List of Items questions, see 
          [DSW specificities](../questionnaire_processing/#ListOfItemsQuestion)


## Naming rules for the mapping annotations

Recall that an annotation in our case is a `(key:value)` pair with `key = interop` (see [Rule 4.](#rule-key)). So here we discuss the naming rules for the value name. 
_Note_: that this is still subject to revisions: it is WIP.

- **Rule 1**. The value names use PascalCase (see [wikipedia](https://en.wikipedia.org/wiki/Naming_convention_(programming))).
- ... TBD: is it possible to establish any other rule?

## Which annotation where?

At this stage, there are no constraints as to what should be annotated.
It is hoped that eventually we'll be able to conceptualise this process. 
But see this discussion about [List of Items questions](../questionnaire_processing/#dealing-with-list-of-items-questions).
    
