    
The `RDA-DMP-CS DMP output; DMP-OPIDoR output`
is where all processing occurs in order to create
a `json` object able to be read/consumed 
by DMP-OPIDoR.
It is a specific model, written for the knowledge model developed within the IFB MUDI4LS project
    (described
[here](https://ifb-elixirfr.gitlab.io/fair/dsw-ifb-instance/)).

## Output options

There are three of them, each constructed on the fly when
processing the questionnaire through its annotation.

1. The list of `[child, parent]` relations
2. The `annotation` tree as a `json` object
3. The final `json` object 


### The list of `[child, parent]` relations

This is a list of relations `[child, parent]`: `parent` is an array, that is 
`[x,[y,z]]` implies that `x` is a child of both `y` and `z`. 
This is possible once one understands how List of Items 
questions work (see [here](../questionnaire_processing/##ListQuestions)).

Recall from [this discussion](../questionnaire_processing/#ChildParentRelation) that hierarchical relationships need to be verified 
when dealing with List of Items questions.
Recall that this output only exists for debugging purposes; an
example looks like (ignore the first line, this is a debugging dump):


<img src="../assets/images/child_of_relations.png" />


### The annotation tree

The tree is a `json` representation of the questionnaire 
where annotations are the keys in the `[key:value]` pairs of the tree.
The tree hierarchy is that of the questionnaire.

<img src="../assets/images/annotation_tree.png"/>

#### Caveat

At the time of writing (July 2024), the tree is a representation of the questionnaire 
where all annotations are shown, even those of questions without answers.
This explains why many `json` properties have empty values.

It is hoped we clean this representation to instead show two trees: one showing only (all) the annotations
embedded in the questionnaire hierarchy, and the other only showing the subset of the latter where answers
exist.

In the example below, we see how empty properties (red) co-exist with answers (green):

<img src="../assets/images/annotation_tree_with_empty_properties.png"/>

#### Question(s)

Why do we see this type of structure:

<img src="../assets/images/followup_annotation_in_tree.png"/>

This occurs because `ContributorIdType ` annotates an option question and a follow-up question of one
option.


### The output `json` object

This the `json` object we create using knowledge model 
annotations as a mapping towards either RDA-DMP-CS or the 
DMP-OPIDoR data dictionnary. 
At the time of writing, only the proof of concept for RDA-DMP-CS 
is operational (albeit with quite a few mistakes for now...):

<img src="../assets/images/rda_dmp_cs_output.png" />

We won't say much more about this output for now. This is WIP.
