<span style="color:Red"><b>Work In Progress</b></span>

Below you'll find a few technical notes about code organisation in 
the project, and a few best practice coding rules. The project is written
in [`jinja2`](https://jinja.palletsprojects.com/en/latest/intro/): `jinja2` 
is not a programming language _per se_, but a text
replacement language. Hence there are macros but not functions, 
macros cannot return values, etc... To circumvent `jinja2`'s shortcomings 
as a programming language we adopt a few 'tricks', mostly to do with 
the use of global variables.


### Caveat

As I am not a `jinja2` expert, you may find some inconsistencies or errors: 
if you could report them as issues in this repository, that would be great. Thank you!


## Handling global variables 

#### Namespaces

In order to ensure a truly global variable scope (read about inner loop scope
in `jinja2`), all global variables are defined within a [namespace](https://jinja.palletsprojects.com/en/3.1.x/templates/#jinja-globals.namespace). 
We cite
> The main purpose of this is to allow carrying a value from within a loop body to an outer scope.

So typically, globals are defined as 
```
{%- set global_ns = namespace(description="globals") -%}
{%- set global_ns.some_global = [] -%}
```

#### Naming namespaces

There are two different types of globals for our purposes: 

1. The ones meant to be accessed/modified from a 
   different file than the one where they are 
   defined. They will be defined within a namespace 
   called `global_ns`. 
   All the namespaces named `global_ns` 
   will be distinguished by the importing context.

2. The global variables that are strictly local to a file will be 
   defined within a namespace called `local_ns`.

This convention's only purpose is code clarity.

#### Persistent variables: The `_global_variables.j2` file

For the purpose of this project, we define persistent variables to be variables 
whose lifetime (or "extent") is the entire run of the program (see 
[here](https://en.wikipedia.org/wiki/Static_variable)). In a traditional programming
language, this definition actually defines static variables. 
However such a concept does not exist 
in `jinja2`, hence the use of the term "persistent".

In order to streamline the code, all the persistent variables that 
need to be seen by one or more files other than the one where they have been defined are 
gathered in the  `_global_variables.j2` file.
There is one exception to this rule: the variable 
`dmp_being_built`
is defined in the main file and accessed through parameter passing.

If a persistent variable is only accessed by one other file
from the one where it is defined (through the `import with context` call), then this is fine
as the variable lives in one context only. However care must be taken when the variable
needs to be seen by more than one file.
This is explained in the following diagram:

<img src="../assets/images/jinja2_context.png" style="height: 500px; width:800px;" />


The file  `_global_variables.j2` can only be imported (`with context`) 
_once_, 
here by `main.json.j2`. Importing it into
another file will create another import context, totally unrelated to the
one in  `main.json.j2`. If a variable is being updated in one context, this cannot
be reflected in the other context.

Therefore, the rule is that 

1. the file  `_global_variables.j2` is _only_ imported once by the main file; 

2. only the main file can import this file;

3. and any other access to the global variables 
is effected through parameter passing. 

#### Organisation of globals in `_global_variables.j2`

1. The namespace is simply called `global_ns` (see [here](#namespaces)
   and  [here](#naming-namespaces)) <br> 
   `   {%- set global_ns = namespace(description="globals") -%}`

2. Everything else is vaguely organised, it would need some better constructed thought...

#### Other global variables: non persistent variables

Global variables may be defined in any file, as long as they need not be persistent. 
A case in point
is the variable `var_stack` defined in `_utils.j2`

```
{%- set global_ns = namespace(description="for_object_creation") -%}
{%- set global_ns.var_stack = [] -%}
```

The `var_stack` variable is used as a means of returning a value from a utility macro
defined in `_utils.j2` (recall that the concept of a return value does not exist in `jinja2`).
How is it typically used?

Assuming that in file `_file1.j2`we have 
```
{%- import "_utils.j2" as utils with context -%} {#- here we are in _file1.j2 -#}
```
with a call to some utility macro `macro_foo` defined in  `_utils.j2`. Further
assume that `macro_foo` 'returns' a variable `var`: it will be pushed
on `global_ns.var_stack` by `macro_foo`:
```
{%- do global_ns.var_stack.append(var) -%} {#- here we are in _utils.j2 -#}
```
This same `var` can then be recovered after the call to `macro_foo`  in `_file1.j2`
by popping `utils.global_ns.var_stack`:
```
{%- import "_utils.j2" as utils with context -%}  {#- here we are in _file1.j2 -#}

{#- call macro_foo -#}
utils.macro_foo()
{#- recover 'return value' -#}
{%- set obj = utils.global_ns.var_stack.pop() -%}
```

So the lifetime of `global_ns.var_stack` is the lifetime of the call 
to  `macro_foo`; it is the equivalent of an automatic variable 
in a standard programming language. 
Therefore  `_utils.j2` can be safely imported 
(each time with a different context) into different files, as the lifetime
of  `global_ns.var_stack`  does not exceed that of a call to a macro
in `_utils.j2`:



<img src="../assets/images/automatic_vars.png" style="height: 500px; width:800px;" />




## Macro and variable names

By convention

* macro names use camelCase
* variable names use snake_case
